//
//  UalaTestTests.swift
//  UalaTestTests
//
//  Created by Macbook on 1/16/21.
//

import XCTest
@testable import UalaTest

class UalaTestTests: XCTestCase {
    
    let mainViewController = RecetasViewController()
    var arrayCount = 0
    
    
    func testInitialSetUp() throws {
        XCTAssertEqual(mainViewController.headerTitle, "Morning", "Should be Morning")
        mainViewController.changeHeaderTitle()
        XCTAssertEqual(mainViewController.headerTitle, "Afternoon", "Should be Afternoon")
        
    }
    
    func testAPIResponse(){
        let expectation = self.expectation(description: "getting")
        WebServices.geRecetas(search: "") { (meals, error) in
            
            guard let meals = meals?.meals?.count else { return }
            self.arrayCount = meals
            expectation.fulfill()
        }
        waitForExpectations(timeout: 5, handler: nil)
        
        // This test is going to fail an propouse
        XCTAssertEqual(self.arrayCount, 20)
    }
}
