//
//  RecetaTableViewCell.swift
//  UalaTest
//
//  Created by Emmanuel Cepeda on 1/16/21.
//

import UIKit
import SDWebImage

class RecetaTableViewCell: UITableViewCell {
    
    @IBOutlet weak var recetaBackView: UIView!
    @IBOutlet weak var recetaImageView: UIImageView!
    @IBOutlet weak var categoryLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    
    func configuration(model: Meal?){
        guard let receta = model else {
            //TODO: Handler alert to let user know that something has failed.
            return
        }
        
        categoryLabel.text = receta.strCategory
        nameLabel.text = receta.strMeal
        recetaImageView?.sd_setImage(with: URL(string: receta.strMealThumb ?? ""))
    }
}
