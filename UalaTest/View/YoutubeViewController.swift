//
//  YoutubeViewController.swift
//  UalaTest
//
//  Created by Macbook on 1/16/21.
//

import UIKit
import youtube_ios_player_helper

    class YoutubeViewController: UIViewController, YTPlayerViewDelegate{
        
        var youtubeId : String = ""
        
        @IBOutlet weak private var youtubeView: YTPlayerView!
        
        override func viewDidLoad() {
            super.viewDidLoad()
            self.youtubeView.delegate = self
            self.youtubeView.load(withVideoId: String(youtubeId))
            self.youtubeView.playVideo()
        }
        override func viewWillAppear(_ animated: Bool) {
            super.viewWillAppear(animated)

            self.navigationItem.title =  Constants.strings.youTubeNavegationBarTitle
        }
    }
