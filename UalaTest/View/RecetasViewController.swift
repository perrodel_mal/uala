//
//  RecetasViewController.swift
//  UalaTest
//
//  Created by Emmanuel Cepeda on 1/16/21.
//

import UIKit

final class RecetasViewController: UIViewController{
    
    //MARK: Properties
    lazy var searchBar:UISearchBar = UISearchBar()
    var meals : [Meal] = []
    var mainViewModel : MainViewModel?
    var headerTitle = "Morning"
    var mealImage : UIImage?
    
    @IBOutlet weak var tableView: UITableView!
    
    //MARK: View life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
    
        registerCell()
        mainViewModel = MainViewModel(model: Constants.strings.viewModelInit)
        mainViewModel?.delegate = self
        tableviewSetUp()
        searchbarSetUp()
        
    }
    
    //MARK: Public methods
    func changeHeaderTitle(){
        headerTitle = "Afternoon"
    }
    
    
    //MARK: Private methods
    private func searchbarSetUp() {
        searchBar.searchBarStyle = UISearchBar.Style.prominent
        searchBar.placeholder = Constants.strings.searchPlaceHolder
        searchBar.sizeToFit()
        searchBar.isTranslucent = false
        searchBar.backgroundImage = UIImage()
        searchBar.delegate = self
        navigationItem.titleView = searchBar
    }
    
    private func tableviewSetUp(){
        tableView.delegate = self
        tableView.dataSource = self
    }
    
    private func registerCell(){
        tableView.register(UINib.init(nibName: Constants.strings.recetaTableViewCell, bundle: nil), forCellReuseIdentifier: Constants.strings.mealforCellReuseIdentifier)

    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == Constants.strings.showDetail {
            let indexPath = self.tableView!.indexPathForSelectedRow
            let detailViewController = segue.destination as! DetailViewController
            detailViewController.meal = meals[indexPath?.row ?? 0]
        }
    }
    
}

extension RecetasViewController: ViewControllerDelegate {
    func getInfo(result: Meals?) {
        
        guard let meals = result?.meals else {
            //TODO: Handler alert.
            return
        }
        self.meals = meals
        DispatchQueue.main.async {
            self.tableView.reloadData()
        }
    }
}

extension RecetasViewController: UISearchBarDelegate, UISearchDisplayDelegate, UISearchResultsUpdating{
    
    func updateSearchResults(for searchController: UISearchController) {
        print("TEST")
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        mainViewModel?.getAPIInformation(searchMeal: searchText)
    }
}

extension RecetasViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.meals.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: Constants.strings.mealforCellReuseIdentifier, for: indexPath) as? RecetaTableViewCell
        cell?.configuration(model: meals[indexPath.row])
        mealImage = cell?.recetaImageView?.image ?? UIImage()
      
        return cell ?? UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.performSegue(withIdentifier: Constants.strings.showDetail , sender: self)
    }
    

}



