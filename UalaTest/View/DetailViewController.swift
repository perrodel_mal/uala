//
//  DetailViewController.swift
//  UalaTest
//
//  Created by Juan Emmanuel Cepeda on 1/16/21.
//

import UIKit

class DetailViewController: UIViewController {
    
    //MARK: Properties
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var mealImagenView: UIImageView!
    @IBOutlet weak var mealNameLabel: UILabel!
    
    var meal: Meal?
    var ingredients : [String?] = []
    
    //MARK: View life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configureViews()
        setupTableview()
    }
    
    //MARK: Public methods
    private func configureViews() {
        guard let meal = self.meal else { return }
        mealNameLabel.text = meal.strMeal
        mealImagenView?.sd_setImage(with: URL(string: meal.strMealThumb ?? ""))
        ingredients = [meal.strIngredient1, meal.strIngredient2, meal.strIngredient3, meal.strIngredient4, meal.strIngredient5, meal.strIngredient6, meal.strIngredient7, meal.strIngredient8, meal.strIngredient9, meal.strIngredient10, meal.strIngredient11, meal.strIngredient12, meal.strIngredient13, meal.strIngredient14, meal.strIngredient15, meal.strIngredient16, meal.strIngredient17, meal.strIngredient18, meal.strIngredient19, meal.strIngredient20]
        tableView.reloadData()
        
    }
    
    func setupTableview(){
        tableView.delegate = self
        tableView.dataSource = self
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == Constants.strings.youTubeNavegationBarTitle {
            let youtubeViewController = segue.destination as! YoutubeViewController
            youtubeViewController.youtubeId = meal?.strYoutube ?? ""
        }
    }
}

extension DetailViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return ingredients.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = UITableViewCell()
        cell.textLabel?.text = ingredients[indexPath.row]
        
        return cell
    }
}
