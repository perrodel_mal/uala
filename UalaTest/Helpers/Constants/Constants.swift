//
//  Constants.swift
//  UalaTest
//
//  Created by Emmanuel Cepeda on 1/16/21.
//

import Foundation

struct Constants {
    
    struct url {
        static let baseUrl = "https://www.themealdb.com/api/json/v1/1/"
        static let searchURL = "search.php?s="
        static let deatailURL = "lookup.php?i="
        static let randomURL = "search.php?s="
    }
    
    struct strings {
    
        static let searchPlaceHolder = " Search..."
        static let viewModelInit = "init"
        static let mealforCellReuseIdentifier = "cell"
        static let recetaTableViewCell = "RecetaTableViewCell"
        static let showDetail = "showDetail"
        static let youTubeNavegationBarTitle = "Youtube"
    }
    
}
