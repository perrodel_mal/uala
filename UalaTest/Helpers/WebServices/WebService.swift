//
//  WebService.swift
//  UalaTest
//
//  Created by Macbook on 1/16/21.
//

import Foundation
import UIKit


// MARK: Web Service
class WebServices: NSObject {
    
    class func geRecetas(search: String, _ completionHandler:@escaping ( _ Array: Meals?, _ err: Error? ) -> Void) {
        
        //TODO: Handler error according.
        
        let urlString = Constants.url.baseUrl + Constants.url.searchURL + search
        guard let url = URL(string: urlString) else { return }
        
        URLSession.shared.dataTask(with: url) { (data, response, error) in
            
            guard let data = data else { return }
        
            do {
                let show = try JSONDecoder().decode(Meals.self, from: data)
                completionHandler(show,nil)
            }catch let jsonError {
                print(jsonError)
                completionHandler(nil,jsonError)
            }
        }.resume()
    }
}

