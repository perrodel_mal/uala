//
//  ViewModel.swift
//  UalaTest
//
//  Created by Emmanuel Cepeda on 1/16/21.
//

protocol ViewControllerDelegate {
    func getInfo(result: Meals?)
}

import Foundation

class MainViewModel {
    
    var model = ""
    var delegate : ViewControllerDelegate?
    
    init(model: String) {
        self.model = model
    }
    
    func getAPIInformation(searchMeal: String?) {
     
        WebServices.geRecetas(search: searchMeal ?? "", { (recetasArray, error) in
            if error == nil {
                self.delegate?.getInfo(result: recetasArray)
            }
        })
    }
}
